import "reflect-metadata";
import http from "http";
import winston from "winston";
import colors from "colors";
import express, {Application} from "express";
import {Connection, createConnection} from "typeorm";
import {applyMiddleware, applyRoutes} from "./utils";
import middleware from "./middleware";
import errorHandlers from "./middleware/errorHandlers";
// import routes from "./services";
import Synox from "./Synox";

colors.enable();

const logger = winston.createLogger({
    level: 'info',
    format: winston.format.cli(),
    defaultMeta: {
        service: 'synox'
    },
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({filename: './logs/error.log', level: 'error'}),
        new winston.transports.File({filename: './logs/combined.log'})
    ]
});

process.on("uncaughtException", e => {
    console.log(e);
    process.exit(1);
});

process.on("unhandledRejection", e => {
    console.log(e);
    process.exit(1);
});

createConnection().then(async (connection: Connection) => {
    logger.info(connection.isConnected ? "Database connected" : "Database not connected !");

    const app: Application = express();
    Synox._initialize(app, connection, logger);

    applyMiddleware(middleware);
    await import('./services/index');
    applyMiddleware(errorHandlers);

    const { PORT = 55555 } = process.env;
    const server = http.createServer(app);

    server.listen(PORT, () =>
        logger.info(`Server is running http://localhost:${PORT}...`)
    );
}).catch(error => console.log(error));

