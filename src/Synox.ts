import {Application} from "express";
import {Connection} from "typeorm";
import {Logger} from "winston";

export default class Synox {

    private static logger: Logger;
    private static app: Application;
    private static database: Connection;

    public static _initialize(app: Application, database: Connection, logger: Logger) {
        this.app = app;
        this.database = database;
        this.logger = logger;
    }

    static getExpress(): Application{
        return this.app;
    }

    static getDatabase(): Connection{
        return this.database;
    }

    static getLogger(): Logger{
        return this.logger;
    }

}
