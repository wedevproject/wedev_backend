import {NextFunction, Request, Response} from "express";
import Synox from "../Synox";
import {HTTP400Error} from "./httpErrors";
import {QueryFailedError} from "typeorm";

type Wrapper = (() => void);

export const applyMiddleware = (
    middlewareWrappers: Wrapper[]
) => {
    for (const wrapper of middlewareWrappers) {
        wrapper();
    }
};

type Handler = (
    req: Request,
    res: Response,
    next: NextFunction
) => Promise<void> | void;

type Route = {
    path: string;
    method: string;
    handler: Handler | Handler[];
};

export const applyRoutes = (routes: Route[]) => {
    for (const route of routes) {
        const { method, path, handler } = route;
        (Synox.getExpress() as any)[method](path, handler);
    }
};

export async function parseID(idString: string): Promise<number> {
    const id: number = parseInt(idString, 10);
    if (isNaN(id)) throw new HTTP400Error('id isn\'t valid');
    return id;
}

export async function save(entity: any){
    try {
        return await Synox.getDatabase().manager.save(entity);
    }catch (e) {
        if (e instanceof QueryFailedError)
            throw new HTTP400Error(e.message);
        throw new HTTP400Error(e.toString());
    }
}
