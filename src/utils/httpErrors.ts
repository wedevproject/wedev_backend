type Error = {
    status: number;
    error: string;
    message: string;
}

export abstract class HTTPClientError extends Error {
    readonly statusCode!: number;
    readonly name!: string;

    protected constructor(message: string) {
        super(message);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }

    toJson(): Error{
        return {
            status: this.statusCode,
            error: this.name,
            message: this.message
        }
    }
}

export class HTTP400Error extends HTTPClientError {
    readonly statusCode = 400;
    constructor(message: string | string[] = "Bad Request") {
        super(Array.isArray(message) ? message.join('. ') : message);
    }
}

export class HTTP404Error extends HTTPClientError {
    readonly statusCode = 404;

    constructor(message: string = "Not found") {
        super(message);
    }
}
