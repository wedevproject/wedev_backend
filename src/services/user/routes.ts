import {checkParams} from "../../middleware/checks";
import {NextFunction, Request, Response} from "express";
import Synox from "../../Synox";
import {delUser, editUserByID, getUsers, loginUser, registerUser, safeDeleteUser} from "./UserController";
import {getUser} from "./providers/UserAuthProvider";
import {parseID} from "../../utils";


/**
 * @swagger
 * /user:
 *   put:
 *     tags:
 *       - User
 *     name: Create
 *     summary: Create in a user
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/User'
 *     responses:
 *       200:
 *         description: User create in successfully
 *         schema:
 *           $ref: '#/definitions/User'
 *       400:
 *         description: Missing parameters
 */
const requiredField_createUser = ['rankId', 'profileId', 'stateSocietyId', 'first_name', 'last_name', 'society', 'email', 'phone', 'password', 'password_confirmed', 'siret'];
Synox.getExpress().put('/api/v1/user', [
	(req: Request, res: Response, next: NextFunction) => checkParams(requiredField_createUser, req, res, next),
	async ({ body }: Request, res: Response) => {
		const result = await registerUser(body);
		res.status(200).send(result);
	}
]);

/**
 * @swagger
 * /user/login:
 *   post:
 *     tags:
 *       - User
 *     name: Login
 *     summary: Login a user
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           type: object
 *           properties:
 *             email:
 *               type: string
 *             password:
 *               type: string
 *               format: password
 *         required:
 *           - username
 *           - password
 *     responses:
 *       200:
 *         description: User create in successfully
 *         schema:
 *           type: object
 *           properties:
 *             token: string
 *       400:
 *         description: Missing parameters
 */
const requiredField_loginUser = ['email', 'password'];
Synox.getExpress().post('/api/v1/user/login', [
	(req: Request, res: Response, next: NextFunction) => checkParams(requiredField_loginUser, req, res, next),
	async ({ body }: Request, res: Response) => {
		const result = await loginUser(body);
		res.status(200).send(result);
	}
]);

/**
 * @swagger
 * /user/{id}:
 *   get:
 *     tags:
 *       - User
 *     name: Get
 *     summary: Get a user by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the user to get
 *
 *     responses:
 *       200:
 *         description: Getting user in successfully
 *         schema:
 *           $ref: '#/definitions/User'
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: User not found
 */
Synox.getExpress().get('/api/v1/user/:id', [
	async (req: Request, res: Response) => res.status(200).send(await getUser(await parseID(req.params.id), ['rank', 'profile', 'stateSociety']))
]);

/**
 * @swagger
 * /user:
 *   get:
 *     tags:
 *       - User
 *     name: Get
 *     summary: Get all users
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Get all users in successfully
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/User'
 *       400:
 *         description: Missing parameters
 */
Synox.getExpress().get('/api/v1/user', [
	async (req: Request, res: Response) => res.status(200).send(await getUsers())
]);

/**
 * @swagger
 * /user/{id}:
 *   post:
 *     tags:
 *       - User
 *     name: Edit
 *     summary: Edit a user by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the user to get
 *
 *     responses:
 *       200:
 *         description: Edit user in successfully
 *         schema:
 *           $ref: '#/definitions/User'
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: User not found
 */
Synox.getExpress().post('/api/v1/user/:id', [
	async (req: Request, res: Response) => res.status(200).send(await editUserByID(await parseID(req.params.id), req.body))
]);

/**
 * @swagger
 * /user/{id}:
 *   delete:
 *     tags:
 *       - User
 *     name: Delete
 *     summary: Delete a user by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the user to delete
 *
 *     responses:
 *       200:
 *         description: Delete user in successfully
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: User not found
 */
Synox.getExpress().delete('/api/v1/user/:id', [
	async (req: Request, res: Response) => res.status(200).send(await safeDeleteUser(await parseID(req.params.id)))
]);

/**
 * @swagger
 * /user/{id}/force:
 *   delete:
 *     tags:
 *       - User
 *     name: Delete
 *     summary: Delete a user by id completely
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the user to delete completely
 *
 *     responses:
 *       200:
 *         description: Delete user in successfully
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: User not found
 */
Synox.getExpress().delete('/api/v1/user/:id/force', [
	async (req: Request, res: Response) => res.status(200).send(await delUser(await parseID(req.params.id)))
]);
