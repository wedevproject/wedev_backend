import {HTTP400Error, HTTP404Error} from "../../utils/httpErrors";
import {User} from "../../entity/User";
import {validate} from "class-validator";
import {createUser, findUser} from "./providers/UserNotAuthProvider";
import jwt from "jsonwebtoken";
import {privateKey} from "../../middleware/jwt";
import {Rank} from "../../entity/Rank";
import {ValidationError} from "class-validator/validation/ValidationError";
import {Profile} from "../../entity/Profile";
import {StateSociety} from "../../entity/StateSociety";
import {deleteObject, editObject, getObjects} from "../ObjectProvider";
import {disableUser, editUser, getUser} from "./providers/UserAuthProvider";

export const registerUser = async (query: UserRegistration) => {
	if (query.password !== query.password_confirmed) {
		throw new HTTP400Error("Password aren't same")
	}
	const user = new User();
	user.firstName = query.first_name;
	user.lastName = query.last_name;
	user.email = query.email;
	user.company = query.society;
	user.phone = query.phone;
	user.password = query.password;
	user.siret = query.siret;
	user.isActive = true;

	const errors: ValidationError[] = await validate(user);
	if (errors.length > 0)
		throw new HTTP400Error(Object.values(errors[0].constraints));

	if (query.rankId) {
		const rank = new Rank();
		rank.id = query.rankId;
		user.rank = rank;
	}

	if (query.profileId) {
		const profile = new Profile();
		profile.id = query.profileId;
		user.profile = profile;
	}

	if (query.stateSocietyId) {
		const stateSociety = new StateSociety();
		stateSociety.id = query.stateSocietyId;
		user.stateSociety = stateSociety;
	}

	return await createUser(user);
};

export const loginUser = async (query: UserLogin): Promise<string> => {
	const user = await findUser(query);
	if (!user) {
		throw new HTTP400Error("Invalid password or username");
	}
	return jwt.sign({
		user_id: user.id,
		user_first_name: user.firstName,
		user_last_name: user.lastName
	}, privateKey, {algorithm: 'RS256'});
};

export const editUserByID = async (id: number, query: UserRegistration) => {
	// const resultUser = await getUser(id);
	// if (!resultUser)
	// 	throw new HTTP404Error('User not found');
	//
	// if (query.rankId) {
	// 	const rank = new Rank();
	// 	rank.id = query.rankId;
	// 	resultUser.rank = rank;
	// }
	//
	// if (query.profileId) {
	// 	const profile = new Profile();
	// 	profile.id = query.profileId;
	// 	resultUser.profile = profile;
	// }
	//
	// if (query.stateSocietyId) {
	// 	const stateSociety = new StateSociety();
	// 	stateSociety.id = query.stateSocietyId;
	// 	resultUser.stateSociety = stateSociety;
	// }
	//
	// Object.keys(resultUser).map((value: string) => (resultUser as any)[value] = (query as any)[value] ? (query as any)[value] : (resultUser as any)[value]);
	//
	// return await editUser(resultUser);

	return await editObject(User, id, query);
};

export const safeDeleteUser = async (id: number) => disableUser(id);
export const delUser = async (id: number) => deleteObject(User, id);
export const getUsers = async () => getObjects(User, ['rank', 'profile', 'stateSociety']);

