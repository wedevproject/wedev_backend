type UserRegistration = {
	first_name: string;
	last_name: string;
	society: string;
	email: string;
	phone: string;
	state_society: string;
	siret: number;
	password: string;
	password_confirmed: string;
	rankId: number;
	stateSocietyId: number;
	profileId: number;
}

type UserLogin = {
	email: string;
	password: string;
}
