import {User} from "../../../entity/User";
import Synox from "../../../Synox";
import {save} from "../../../utils";

export const createUser = async (user: User): Promise<User> => {
	return save(user);
};

export const findUser = async (user: UserLogin): Promise<User | undefined> => {
	return await Synox.getDatabase().getRepository<User>(User).findOne({
		where: {
			email: user.email,
			password: user.password
		}
	});
};
