import {User} from "../../../entity/User";
import Synox from "../../../Synox";

export const getUser = async (id: number, relations: string[] = []): Promise<User | undefined> => {
    return await Synox.getDatabase().getRepository<User>(User).findOne({
        relations,
        where:{
            id
        }
    });
};

export const editUser = async (user: User): Promise<User> => {
    return Synox.getDatabase().manager.save(user);
};

export const disableUser = async (id: number): Promise<User | undefined> => {
    const user: User | undefined = await Synox.getDatabase().getRepository<User>(User).findOne({
        id,
    });
    if (user) {
        user.isActive = false;
        return Synox.getDatabase().manager.save(user);
    }

    return user;
};
