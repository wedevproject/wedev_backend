import {ValidationError} from "class-validator/validation/ValidationError";
import {validate} from "class-validator";
import {HTTP400Error} from "../../utils/httpErrors";
import Synox from "../../Synox";
import {Sprint} from "../../entity/Sprint";
import {editObject, getObject, getObjects} from "../ObjectProvider";
import {SprintRegistration, TaskRegistration} from "./types";
import {Task} from "../../entity/Task";
import {User} from "../../entity/User";
import {QueryFailedError} from "typeorm";
import {save} from "../../utils";

/**
 * ███████ ██████  ██████  ██ ███    ██ ████████
 * ██      ██   ██ ██   ██ ██ ████   ██    ██
 * ███████ ██████  ██████  ██ ██ ██  ██    ██
 *      ██ ██      ██   ██ ██ ██  ██ ██    ██
 * ███████ ██      ██   ██ ██ ██   ████    ██
 */
export const registerSprint = async (query: SprintRegistration): Promise<Sprint> => {
    const sprint = new Sprint();
    sprint.title = query.title;
    sprint.starting_date = query.starting_date;
    sprint.ending_date = query.ending_date;
    sprint.state = query.state;

    const errors: ValidationError[] = await validate(sprint);
    if (errors.length > 0)
        throw new HTTP400Error(Object.values(errors[0].constraints));

    return save(sprint);
};

export const getSprint = async (id: number) => await getObject(Sprint, id, ['project', 'tasks']);
export const getSprints = async () => await getObjects(Sprint, ['project', 'tasks']);
export const editSprint = async (id: number, query: SprintRegistration) => await editObject(Sprint, id, query);

/**
 * ████████  █████  ███████ ██   ██
 *    ██    ██   ██ ██      ██  ██
 *    ██    ███████ ███████ █████
 *    ██    ██   ██      ██ ██  ██
 *    ██    ██   ██ ███████ ██   ██
 */

export const registerTask = async (query: TaskRegistration): Promise<Task> => {
    const task = new Task();
    task.title = query.title;
    task.description = query.description;
    task.time = query.time;

    const errors: ValidationError[] = await validate(task);
    if (errors.length > 0)
        throw new HTTP400Error(Object.values(errors[0].constraints));

    return save(task);
};

export const getTask = async (id: number) => await getObject(Task, id, ['sprint']);
export const getTasks = async () => await getObjects(Task, ['sprint']);
export const editTask = async (id: number, query: TaskRegistration) => await editObject(Task, id, query);
