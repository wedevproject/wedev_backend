import {State} from "../../entity/State";

export type SprintRegistration = {
    title: string;
    starting_date: Date;
    ending_date: Date;
    state: State
}

export type TaskRegistration = {
    title: string;
    state: State;
    description: string;
    time: number;
}
