import Synox from "../../Synox";
import {NextFunction, Request, Response} from "express";
import {checkParams} from "../../middleware/checks";
import {State} from "../../entity/State";
import {parseID} from "../../utils";
import {
    editSprint,
    editTask,
    getSprint,
    getSprints,
    getTask,
    getTasks,
    registerSprint,
    registerTask
} from "./PlanningCrontroller";
import {deleteObject} from "../ObjectProvider";
import {Sprint} from "../../entity/Sprint";
import {Task} from "../../entity/Task";

/**
 * @swagger
 * /sprint:
 *   put:
 *     tags:
 *       - Sprint
 *     name: Create
 *     summary: Create in a sprint
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Sprint'
 *     responses:
 *       200:
 *         description: Sprint create in successfully
 *         schema:
 *           $ref: '#/definitions/Sprint'
 *       400:
 *         description: Missing parameters
 */
const requiredField_createSprint = ['title', 'starting_date', 'ending_date', 'state'];
Synox.getExpress().put('/api/v1/sprint/', [
    (req: Request, res: Response, next: NextFunction) => checkParams(requiredField_createSprint, req, res, next),
    async ({body}: Request, res: Response) => {

        body.state = (<any>State)[body.state];
        body.ending_date = new Date(body.ending_date);
        body.starting_date = new Date(body.starting_date);

        const result = await registerSprint(body);
        res.status(200).send(result);
    }
]);

/**
 * @swagger
 * /sprint/{id}:
 *   get:
 *     tags:
 *       - Sprint
 *     name: Get
 *     summary: Get a sprint by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the user to get
 *
 *     responses:
 *       200:
 *         description: Getting sprint in successfully
 *         schema:
 *           $ref: '#/definitions/Sprint'
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: Project not found
 */
Synox.getExpress().put('/api/v1/sprint/:id', [
    async (req: Request, res: Response) => res.status(200).send(await getSprint(await parseID(req.params.id)))
]);

/**
 * @swagger
 * /sprint:
 *   get:
 *     tags:
 *       - Sprint
 *     name: Get
 *     summary: Get all sprints
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Get all sprints in successfully
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Sprint'
 *       400:
 *         description: Missing parameters
 */
Synox.getExpress().get('/api/v1/sprint', [
    async (req: Request, res: Response) => res.status(200).send(await getSprints())
]);

/**
 * @swagger
 * /sprint/{id}:
 *   post:
 *     tags:
 *       - Sprint
 *     name: Edit
 *     summary: Edit a sprint by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the sprint to get
 *
 *     responses:
 *       200:
 *         description: Edit sprint in successfully
 *         schema:
 *           $ref: '#/definitions/Sprint'
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: Sprint not found
 */
Synox.getExpress().post('/api/v1/sprint/:id', [
    async (req: Request, res: Response) => res.status(200).send(await editSprint(await parseID(req.params.id), req.body))
]);

/**
 * @swagger
 * /sprint/{id}:
 *   delete:
 *     tags:
 *       - Sprint
 *     name: Delete
 *     summary: Delete a sprint by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the sprint to delete
 *
 *     responses:
 *       200:
 *         description: Delete sprint in successfully
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: Sprint not found
 */
Synox.getExpress().delete('/api/v1/sprint/:id', [
    async (req: Request, res: Response) => res.status(200).send(await deleteObject(Sprint, await parseID(req.params.id)))
]);






/**
 * @swagger
 * /task:
 *   put:
 *     tags:
 *       - Task
 *     name: Create
 *     summary: Create in a task
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Task'
 *     responses:
 *       200:
 *         description: Task create in successfully
 *         schema:
 *           $ref: '#/definitions/Task'
 *       400:
 *         description: Missing parameters
 */
const requiredField_createTask = ['title', 'starting_date', 'ending_date', 'state'];
Synox.getExpress().put('/api/v1/task/', [
    (req: Request, res: Response, next: NextFunction) => checkParams(requiredField_createTask, req, res, next),
    async ({body}: Request, res: Response) => {

        body.state = (<any>State)[body.state];

        const result = await registerTask(body);
        res.status(200).send(result);
    }
]);

/**
 * @swagger
 * /task/{id}:
 *   get:
 *     tags:
 *       - Task
 *     name: Get
 *     summary: Get a task by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the user to get
 *
 *     responses:
 *       200:
 *         description: Getting task in successfully
 *         schema:
 *           $ref: '#/definitions/Task'
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: Project not found
 */
Synox.getExpress().put('/api/v1/task/:id', [
    async (req: Request, res: Response) => res.status(200).send(await getTask(await parseID(req.params.id)))
]);

/**
 * @swagger
 * /task:
 *   get:
 *     tags:
 *       - Task
 *     name: Get
 *     summary: Get all tasks
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Get all tasks in successfully
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Task'
 *       400:
 *         description: Missing parameters
 */
Synox.getExpress().get('/api/v1/task', [
    async (req: Request, res: Response) => res.status(200).send(await getTasks())
]);

/**
 * @swagger
 * /task/{id}:
 *   post:
 *     tags:
 *       - Task
 *     name: Edit
 *     summary: Edit a task by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the task to get
 *
 *     responses:
 *       200:
 *         description: Edit task in successfully
 *         schema:
 *           $ref: '#/definitions/Task'
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: Task not found
 */
Synox.getExpress().post('/api/v1/task/:id', [
    async (req: Request, res: Response) => res.status(200).send(await editTask(await parseID(req.params.id), req.body))
]);

/**
 * @swagger
 * /task/{id}:
 *   delete:
 *     tags:
 *       - Task
 *     name: Delete
 *     summary: Delete a task by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the task to delete
 *
 *     responses:
 *       200:
 *         description: Delete task in successfully
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: Task not found
 */
Synox.getExpress().delete('/api/v1/task/:id', [
    async (req: Request, res: Response) => res.status(200).send(await deleteObject(Task, await parseID(req.params.id)))
]);
