import {Project} from "../../entity/Project";
import {CustomerRegistration, ProjectRegistration} from "./types";
import Synox from "../../Synox";
import {ValidationError} from "class-validator/validation/ValidationError";
import {validate} from "class-validator";
import {HTTP400Error} from "../../utils/httpErrors";
import {editObject, getObject, getObjects} from "../ObjectProvider";
import {Customer} from "../../entity/Customer";
import {save} from "../../utils";
import {User} from "../../entity/User";

export const registerProject = async (query: ProjectRegistration): Promise<Project> => {
    const project = new Project();
    project.title = query.title;
    project.amount = query.amount;
    project.starting_date = query.starting_date;
    project.ending_date = query.ending_date;
    project.state = query.state;
    project.stack = query.stack;
    project.hourly_cost = query.hourly_cost;

    if (query.clientId) {
        const customer = new Customer();
        customer.id = query.clientId;
        project.client = customer;
    }

    if (query.userId) {
        const user = new User();
        user.id = query.userId;
        project.user = user;
    }

    const errors: ValidationError[] = await validate(project);
    if (errors.length > 0)
        throw new HTTP400Error(Object.values(errors[0].constraints));

    return save(project);
};

export const getProject = async (id: number) => await getObject(Project, id, ['sprints']);
export const getProjects = async () => await getObjects(Project);
export const editProject = async (id: number, query: ProjectRegistration) => await editObject(Project, id, query);

export const registerCustomer = async (query: CustomerRegistration): Promise<Customer> => {
    const customer = new Customer();
    customer.corporate_name = query.corporate_name;
    customer.address = query.address;
    customer.first_name = query.first_name;
    customer.last_name = query.last_name;
    customer.phone = query.phone;
    customer.email = query.email;

    const errors: ValidationError[] = await validate(customer);
    if (errors.length > 0)
        throw new HTTP400Error(Object.values(errors[0].constraints));

    return save(customer);
};

export const getCustomer = async (id: number) => await getObject(Customer, id, ['sprints']);
export const getCustomers = async () => await getObjects(Customer);
export const editCustomer = async (id: number, query: ProjectRegistration) => await editObject(Customer, id, query);



