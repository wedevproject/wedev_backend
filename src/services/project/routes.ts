import Synox from "../../Synox";
import {NextFunction, Request, Response} from "express";
import {
    editCustomer,
    editProject,
    getCustomer, getCustomers,
    getProject,
    getProjects,
    registerCustomer,
    registerProject
} from "./ProjectController";
import {checkParams} from "../../middleware/checks";
import {State} from "../../entity/State";
import {parseID} from "../../utils";
import {deleteObject} from "../ObjectProvider";
import {Project} from "../../entity/Project";
import {Customer} from "../../entity/Customer";

/**
 * @swagger
 * /project:
 *   put:
 *     tags:
 *       - Project
 *     name: Create
 *     summary: Create in a project
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Project'
 *     responses:
 *       200:
 *         description: Project create in successfully
 *         schema:
 *           $ref: '#/definitions/Project'
 *       400:
 *         description: Missing parameters
 */
const requiredField_createProfile = ['title', 'amount', 'starting_date', 'ending_date', 'state', 'stack', 'hourly_cost'];
Synox.getExpress().put('/api/v1/project/', [
    (req: Request, res: Response, next: NextFunction) => checkParams(requiredField_createProfile, req, res, next),
    async ({body}: Request, res: Response) => {

        body.state = (<any>State)[body.state];
        body.ending_date = new Date(body.ending_date);
        body.starting_date = new Date(body.starting_date);

        const result = await registerProject(body);
        res.status(200).send(result);
    }
]);


/**
 * @swagger
 * /project/{id}:
 *   get:
 *     tags:
 *       - Project
 *     name: Get
 *     summary: Get a project by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the user to get
 *
 *     responses:
 *       200:
 *         description: Getting user in successfully
 *         schema:
 *           $ref: '#/definitions/Project'
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: Project not found
 */
Synox.getExpress().put('/api/v1/project/:id', [
    async (req: Request, res: Response) => res.status(200).send(await getProject(await parseID(req.params.id)))
]);

/**
 * @swagger
 * /project:
 *   get:
 *     tags:
 *       - Project
 *     name: Get
 *     summary: Get all projects
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Get all projects in successfully
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Project'
 *       400:
 *         description: Missing parameters
 */
Synox.getExpress().get('/api/v1/project', [
    async (req: Request, res: Response) => res.status(200).send(await getProjects())
]);

/**
 * @swagger
 * /project/{id}:
 *   post:
 *     tags:
 *       - Project
 *     name: Edit
 *     summary: Edit a project by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the project to get
 *
 *     responses:
 *       200:
 *         description: Edit project in successfully
 *         schema:
 *           $ref: '#/definitions/Project'
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: User not found
 */
Synox.getExpress().post('/api/v1/project/:id', [
    async (req: Request, res: Response) => res.status(200).send(await editProject(await parseID(req.params.id), req.body))
]);

/**
 * @swagger
 * /project/{id}:
 *   delete:
 *     tags:
 *       - Project
 *     name: Delete
 *     summary: Delete a project by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the project to delete
 *
 *     responses:
 *       200:
 *         description: Delete project in successfully
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: Project not found
 */
Synox.getExpress().delete('/api/v1/project/:id', [
    async (req: Request, res: Response) => res.status(200).send(await deleteObject(Project, await parseID(req.params.id)))
]);







/**
 * @swagger
 * /customer:
 *   put:
 *     tags:
 *       - Customer
 *     name: Create
 *     summary: Create in a customer
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Customer'
 *     responses:
 *       200:
 *         description: Customer create in successfully
 *         schema:
 *           $ref: '#/definitions/Customer'
 *       400:
 *         description: Missing parameters
 */
const requiredField_createCustomer = ['corporate_name', 'address', 'first_name', 'last_name', 'phone', 'email'];
Synox.getExpress().put('/api/v1/customer/', [
    (req: Request, res: Response, next: NextFunction) => checkParams(requiredField_createCustomer, req, res, next),
    async ({body}: Request, res: Response) => res.status(200).send(await registerCustomer(body))
]);


/**
 * @swagger
 * /customer/{id}:
 *   get:
 *     tags:
 *       - Customer
 *     name: Get
 *     summary: Get a customer by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the customer to get
 *
 *     responses:
 *       200:
 *         description: Getting user in successfully
 *         schema:
 *           $ref: '#/definitions/Customer'
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: Customer not found
 */
Synox.getExpress().put('/api/v1/customer/:id', [
    async (req: Request, res: Response) => res.status(200).send(await getCustomer(await parseID(req.params.id)))
]);

/**
 * @swagger
 * /customer:
 *   get:
 *     tags:
 *       - Customer
 *     name: Get
 *     summary: Get all customers
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Get all customers in successfully
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Customer'
 *       400:
 *         description: Missing parameters
 */
Synox.getExpress().get('/api/v1/customer', [
    async (req: Request, res: Response) => res.status(200).send(await getCustomers)
]);

/**
 * @swagger
 * /customer/{id}:
 *   post:
 *     tags:
 *       - Customer
 *     name: Edit
 *     summary: Edit a customer by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the customer to get
 *
 *     responses:
 *       200:
 *         description: Edit customer in successfully
 *         schema:
 *           $ref: '#/definitions/Customer'
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: Customer not found
 */
Synox.getExpress().post('/api/v1/customer/:id', [
    async (req: Request, res: Response) => res.status(200).send(await editCustomer(await parseID(req.params.id), req.body))
]);

/**
 * @swagger
 * /customer/{id}:
 *   delete:
 *     tags:
 *       - Customer
 *     name: Delete
 *     summary: Delete a customer by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the customer to delete
 *
 *     responses:
 *       200:
 *         description: Delete customer in successfully
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: Customer not found
 */
Synox.getExpress().delete('/api/v1/customer/:id', [
    async (req: Request, res: Response) => res.status(200).send(await deleteObject(Customer, await parseID(req.params.id)))
]);
