import {State} from "../../entity/State";

export type ProjectRegistration = {
    title: string;
    amount: number;
    starting_date: Date;
    ending_date: Date;
    state: State;
    stack: string;
    hourly_cost: number;
    clientId: number;
    userId: number;
}

export type CustomerRegistration = {
    corporate_name: string;
    address: string;
    first_name: string;
    last_name: string;
    phone: string;
    email: string;
}
