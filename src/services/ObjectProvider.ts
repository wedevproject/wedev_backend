import Synox from "../Synox";
import {HTTP404Error} from "../utils/httpErrors";
import {save} from "../utils";

/**
 * Récupère tous les objects
 */
export const getObjects = async (obj: any, relations: string[] = []) => {
    return await Synox.getDatabase().getRepository(obj).find({take: 1000, relations});
};

/**
 * Récupère un object depuis une id
 */
export const getObject = async (obj: any, id: number, relations: string[] = []) => {
    const result = await Synox.getDatabase().getRepository(obj).findOne({
        relations,
        where: {
            id
        }
    });
    if (!result) throw new HTTP404Error("Object not found");
    return result;
};

/**
 * Créer un nouvelle object
 */
export const createObject = async (obj: any, newObj: any) => {
    return save(await newObj);

};

/**
 * Supprime un object
 */
export const deleteObject = async (obj: any, id: number) => {
    return await Synox.getDatabase().getRepository(obj).delete({
        id
    });
};

/**
 * Modifier un object
 * @param obj class de l'object a modifier
 * @param id identifiant de l'object a modifier
 * @param query paramètre a modifier
 */
export const editObject = async (obj: any, id: number, query: any) => {
    const resultObj: any = await getObject(obj, id);
    if (!resultObj)
        throw new HTTP404Error('Object not found');

    Object.keys(query).map((value: string) => {
        if (value.includes('Id')) {
            const objName = value.replace('Id', '').charAt(0).toUpperCase() + value.replace('Id', '').slice(1);
            const Obj = require('../entity/'+objName)[objName];
            const obj = new Obj();
            obj.id = (query as any)[value];
            (resultObj as any)[objName.toLowerCase()] = obj;
        }else{
            if (resultObj.hasOwnProperty(value)) {
                (resultObj as any)[value] = (query as any)[value] ? (query as any)[value] : (resultObj as any)[value];
            }
        }
    });
    return await Synox.getDatabase().manager.save(resultObj);
};

