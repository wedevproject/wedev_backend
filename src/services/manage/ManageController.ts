import {Rank} from "../../entity/Rank";
import {validate} from "class-validator";
import {HTTP400Error} from "../../utils/httpErrors";
import {createObject, deleteObject, getObject, getObjects} from "../ObjectProvider";
import {Profile} from "../../entity/Profile";
import {StateSociety} from "../../entity/StateSociety";

export const getRanks = async () => await getObjects(Rank);
export const getRank = async (id: number) => await getObject(Rank, id);
export const registerRank = async (name: string) => await createObject(Rank, validateWithName('Rank', name));
export const delRank = async (id: number) => await deleteObject(Rank, id);

export const getProfiles = async () => await getObjects(Profile);
export const getProfile = async (id: number) => await getObject(Profile, id);
export const registerProfile = async (name: string) => await createObject(Profile, await validateWithName('Profile', name));
export const delProfile = async (id: number) => await deleteObject(Profile, id);

export const getStateSocieties = async () => await getObjects(StateSociety);
export const getStateSociety = async (id: number) => await getObject(StateSociety, id);
export const registerStateSociety = async (name: string) => await createObject(StateSociety, await validateWithName('StateSociety', name));
export const delStateSociety = async (id: number) => await deleteObject(StateSociety, id);

async function validateWithName(object: string, name: string): Promise<any>{
    const Obj = require('../../entity/'+object)[object];
    const instance = new Obj();
    instance.name = name;

    const errors = await validate(instance);
    if (errors.length > 0)
        throw new HTTP400Error(errors.join(", "));

    return instance;
}
