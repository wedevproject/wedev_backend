import Synox from "../../Synox";
import {NextFunction, Request, Response} from "express";
import {checkParams} from "../../middleware/checks";
import {
    delProfile,
    delRank,
    delStateSociety,
    getProfile,
    getProfiles,
    getRank,
    getRanks,
    getStateSocieties,
    getStateSociety,
    registerProfile,
    registerRank,
    registerStateSociety
} from "./ManageController";
import {parseID} from "../../utils";

/**
 * ██████   █████  ███    ██ ██   ██
 * ██   ██ ██   ██ ████   ██ ██  ██
 * ██████  ███████ ██ ██  ██ █████
 * ██   ██ ██   ██ ██  ██ ██ ██  ██
 * ██   ██ ██   ██ ██   ████ ██   ██
 */
/**
 * @swagger
 * /rank:
 *   get:
 *     tags:
 *       - Rank
 *     name: Get
 *     summary: Get all ranks
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Get all ranks in successfully
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Rank'
 *       400:
 *         description: Missing parameters
 */
Synox.getExpress().get('/api/v1/rank', [
    async (req: Request, res: Response) => res.status(200).send(await getRanks())
]);

/**
 * @swagger
 * /rank/{id}:
 *   get:
 *     tags:
 *       - Rank
 *     name: Get
 *     summary: Get a rank by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the rank to get
 *
 *     responses:
 *       200:
 *         description: Getting rank in successfully
 *         schema:
 *           $ref: '#/definitions/Rank'
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: Rank not found
 */
Synox.getExpress().get('/api/v1/rank/:id', [
    async (req: Request, res: Response) => res.status(200).send(await getRank(await parseID(req.params.id)))
]);

/**
 * @swagger
 * /rank/{id}:
 *   delete:
 *     tags:
 *       - Rank
 *     name: Delete
 *     summary: Delete a rank by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the rank to delete
 *
 *     responses:
 *       200:
 *         description: Delete rank in successfully
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: Rank not found
 */
Synox.getExpress().delete('/api/v1/rank/:id', [
    async (req: Request, res: Response) => res.status(200).send(await delRank(await parseID(req.params.id)))
]);

/**
 * @swagger
 * /rank/:
 *   put:
 *     tags:
 *       - Rank
 *     name: Add
 *     summary: Add a rank by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the rank to add
 *
 *     responses:
 *       200:
 *         description: Add rank in successfully
 *         schema:
 *           $ref: '#/definitions/Rank'
 *       400:
 *         description: Missing parameters
 */
Synox.getExpress().put('/api/v1/rank', [
    (req: Request, res: Response, next: NextFunction) => checkParams(['name'], req, res, next),
    async (req: Request, res: Response) => res.status(200).send(await registerRank(req.body.name))
]);


/**
 * ██████  ██████   ██████  ███████ ██ ██      ███████
 * ██   ██ ██   ██ ██    ██ ██      ██ ██      ██
 * ██████  ██████  ██    ██ █████   ██ ██      █████
 * ██      ██   ██ ██    ██ ██      ██ ██      ██
 * ██      ██   ██  ██████  ██      ██ ███████ ███████
 */
/**
 * @swagger
 * /profile:
 *   get:
 *     tags:
 *       - Profile
 *     name: Get
 *     summary: Get all profiles
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Get all profiles in successfully
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Profile'
 *       400:
 *         description: Missing parameters
 */
Synox.getExpress().get('/api/v1/profile', [
    async (req: Request, res: Response) => res.status(200).send(await getProfiles())
]);

/**
 * @swagger
 * /profile/{id}:
 *   get:
 *     tags:
 *       - Profile
 *     name: Get
 *     summary: Get a profile by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the profile to get
 *
 *     responses:
 *       200:
 *         description: Getting profile in successfully
 *         schema:
 *           $ref: '#/definitions/Profile'
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: Profile not found
 */
Synox.getExpress().get('/api/v1/profile/:id', [
    async (req: Request, res: Response) => res.status(200).send(await getProfile(await parseID(req.params.id)))
]);

/**
 * @swagger
 * /profile/{id}:
 *   delete:
 *     tags:
 *       - Profile
 *     name: Delete
 *     summary: Delete a profile by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the profile to delete
 *
 *     responses:
 *       200:
 *         description: Delete profile in successfully
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: Profile not found
 */
Synox.getExpress().delete('/api/v1/profile/:id', [
    async (req: Request, res: Response) => res.status(200).send(await delProfile(await parseID(req.params.id)))
]);

/**
 * @swagger
 * /profile/:
 *   put:
 *     tags:
 *       - Profile
 *     name: Add
 *     summary: Add a profile by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the profile to add
 *
 *     responses:
 *       200:
 *         description: Add profile in successfully
 *         schema:
 *           $ref: '#/definitions/Profile'
 *       400:
 *         description: Missing parameters
 */
Synox.getExpress().put('/api/v1/profile', [
    (req: Request, res: Response, next: NextFunction) => checkParams(['name'], req, res, next),
    async (req: Request, res: Response) => res.status(200).send(await registerProfile(req.body.name))
]);

/**
 * ███████ ████████  █████  ████████ ███████     ███████  ██████   ██████ ██ ███████ ████████ ██    ██
 * ██         ██    ██   ██    ██    ██          ██      ██    ██ ██      ██ ██         ██     ██  ██
 * ███████    ██    ███████    ██    █████       ███████ ██    ██ ██      ██ █████      ██      ████
 *      ██    ██    ██   ██    ██    ██               ██ ██    ██ ██      ██ ██         ██       ██
 * ███████    ██    ██   ██    ██    ███████     ███████  ██████   ██████ ██ ███████    ██       ██
 */
/**
 * @swagger
 * /state_society:
 *   get:
 *     tags:
 *       - State-Society
 *     name: Get
 *     summary: Get all state-Society
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Get all state-Society in successfully
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/StateSociety'
 *       400:
 *         description: Missing parameters
 */
Synox.getExpress().get('/api/v1/state_society', [
    async (req: Request, res: Response) => res.status(200).send(await getStateSocieties())
]);

/**
 * @swagger
 * /state_society/{id}:
 *   get:
 *     tags:
 *       - State-Society
 *     name: Get
 *     summary: Get a state-Society by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the state-Society to get
 *
 *     responses:
 *       200:
 *         description: Getting state-Society in successfully
 *         schema:
 *           $ref: '#/definitions/StateSociety'
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: StateSociety not found
 */
Synox.getExpress().get('/api/v1/state_society/:id', [
    async (req: Request, res: Response) => res.status(200).send(await getStateSociety(await parseID(req.params.id)))
]);

/**
 * @swagger
 * /state-Society/{id}:
 *   delete:
 *     tags:
 *       - State-Society
 *     name: Delete
 *     summary: Delete a state-Society by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the state-Society to delete
 *
 *     responses:
 *       200:
 *         description: Delete state-Society in successfully
 *       400:
 *         description: Missing parameters
 *       404:
 *         description: State-Society not found
 */
Synox.getExpress().delete('/api/v1/state-Society/:id', [
    async (req: Request, res: Response) => res.status(200).send(await delStateSociety(await parseID(req.params.id)))
]);

/**
 * @swagger
 * /state-Society/:
 *   put:
 *     tags:
 *       - State-Society
 *     name: Add
 *     summary: Add a state-Society by id
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - id: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: Numeric ID of the state-Society to add
 *
 *     responses:
 *       200:
 *         description: Add profile in successfully
 *         schema:
 *           $ref: '#/definitions/StateSociety'
 *       400:
 *         description: Missing parameters
 */
Synox.getExpress().put('/api/v1/state-Society', [
    (req: Request, res: Response, next: NextFunction) => checkParams(['name'], req, res, next),
    async (req: Request, res: Response) => res.status(200).send(await registerStateSociety(req.body.name))
]);
