import {Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {IsNumber} from "class-validator";
import {Sprint} from "./Sprint";
import {User} from "./User";

/**
 * @swagger
 * definitions:
 *   Task:
 *     type: object
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       description:
 *         type: string
 *       time:
 *         type: integer
 */

@Entity()
export class Task {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column({length: 200})
    title!: string;

    @Column({type: "text"})
    description!: string;

    @IsNumber()
    @Column()
    time!: number;

    @IsNumber()
    @Column()
    order!: number;

    @JoinColumn()
    @OneToOne(type => Sprint)
    sprints!: Sprint[];

    @JoinColumn()
    @OneToOne(type => User)
    user!: User;
}
