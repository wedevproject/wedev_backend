import {IsDate, IsEnum, IsNumber} from "class-validator";
import {Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {State} from "./State";
import {Project} from "./Project";

/**
 * @swagger
 * definitions:
 *   Sprint:
 *     type: object
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       starting_date:
 *         type: date
 *       ending_date:
 *         type: date
 *       state:
 *         type: string
 *         enum:
 *           - to_do
 *           - in_progress
 *           - done
 */

@Entity()
export class Sprint {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column({length: 200})
    title!: string;

    @IsDate()
    @Column()
    starting_date!: Date;

    @IsDate()
    @Column()
    ending_date!: Date;

    @IsEnum(State)
    @Column({
        type: "enum",
        enum: State,
        default: State.TO_DO
    })
    state!: State;

    @OneToMany(type => Sprint, sprint => sprint.task)
    task!: Sprint[];

    @JoinColumn()
    @OneToOne(type => Project)
    project!: Project;
}
