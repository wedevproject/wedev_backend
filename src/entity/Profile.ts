import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {User} from "./User";
import {IsAlpha, IsNumber} from "class-validator";

/**
 * @swagger
 * definitions:
 *   Profile:
 *     type: object
 *     properties:
 *       id:
 *         type: integer
 *       name:
 *         type: string
 */

@Entity()
export class Profile {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column({length: 100})
    name!: string;

    @OneToMany(type => User, user => user.profile)
    user!: User[]

}
