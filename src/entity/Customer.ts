import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {IsAlpha, IsEmail, IsNumber} from "class-validator";

/**
 * @swagger
 * definitions:
 *   Customer:
 *     type: object
 *     properties:
 *       id:
 *         type: integer
 *       corporate_name:
 *         type: string
 *       address:
 *         type: string
 *       first_name:
 *         type: string
 *       last_name:
 *         type: string
 *       phone:
 *         type: integer
 *       email:
 *         type: string
 */
@Entity()
export class Customer {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column({length: 100})
    corporate_name!: string;

    @Column({length: 100})
    address!: string;

    @IsAlpha()
    @Column({length: 100})
    first_name!: string;

    @IsAlpha()
    @Column({length: 100})
    last_name!: string;

    @IsNumber()
    @Column({length: 10})
    phone!: string;

    @IsEmail()
    @Column({length: 200})
    email!: string
}
