import {Entity, PrimaryGeneratedColumn, Column, OneToMany, JoinColumn, OneToOne} from "typeorm";
import {StateSociety} from "./StateSociety";
import {IsAlpha, IsBoolean, IsEmail, IsNumber, IsNumberString, Length} from "class-validator";
import {Rank} from "./Rank";
import {Profile} from "./Profile";

/**
 * @swagger
 * definitions:
 *   User:
 *     type: object
 *     properties:
 *       id:
 *         type: integer
 *       firstName:
 *         type: string
 *       lastName:
 *         type: string
 *       email:
 *         type: string
 *       phone:
 *         type: number
 *       password:
 *         type: string
 *       company:
 *         type: string
 *       siret:
 *         type: number
 *       isActive:
 *         type: boolean
 */

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id!: number;

    @IsAlpha()
    @Column({length: 100})
    firstName!: string;

    @IsAlpha()
    @Column({length: 100})
    lastName!: string;

    @IsEmail()
    @Column({length: 200, unique: true})
    email!: string;

    @IsNumberString()
    @Column({length: 10})
    phone!: string;

    @Length(7, 200)
    @Column({length: 200})
    password!: string;

    @IsAlpha()
    @Column({length: 100})
    company!: string;

    @IsNumberString()
    @Column("double")
    siret!: number;

    @IsBoolean()
    @Column()
    isActive!: boolean;

    @JoinColumn()
    @OneToOne(type => Profile, profile => profile.user)
    profile!: Profile;

    @JoinColumn()
    @OneToOne(type => StateSociety, stateSociety => stateSociety.user)
    stateSociety!: StateSociety;

    @JoinColumn()
    @OneToOne(type => Rank, rank => rank.user)
    rank!: Rank;
}
