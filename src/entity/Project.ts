import {Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {IsAlpha, IsDate, IsEnum, IsNumber} from "class-validator";
import {State} from "./State";
import {Sprint} from "./Sprint";
import {Customer} from "./Customer";
import {User} from "./User";

/**
 * @swagger
 * definitions:
 *   Project:
 *     type: object
 *     properties:
 *       id:
 *         type: integer
 *       title:
 *         type: string
 *       amount:
 *         type: integer
 *       starting_date:
 *         type: date
 *       ending_date:
 *         type: date
 *       state:
 *         type: string
 *         enum:
 *           - to_do
 *           - in_progress
 *           - done
 *       stack:
 *         type: string
 *       hourly_cost:
 *         type: integer
 */


@Entity()
export class Project {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column({length: 200})
    title!: string;

    @IsNumber()
    @Column()
    amount!: number;

    @IsDate()
    @Column()
    starting_date!: Date;

    @IsDate()
    @Column()
    ending_date!: Date;

    @IsEnum(State)
    @Column({
        type: "enum",
        enum: State,
        default: State.TO_DO
    })
    state!: State;

    @Column({length: 250})
    stack!: string;

    @IsNumber()
    @Column()
    hourly_cost!: number;

    @OneToMany(type => Sprint, sprint => sprint.project)
    sprint!: Sprint[];

    @JoinColumn()
    @OneToOne(type => Customer)
    client!: Customer;

    @JoinColumn()
    @OneToOne(type => User)
    user!: User;
}
