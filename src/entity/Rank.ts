import {Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {User} from "./User";
import {IsAlpha, IsNumber} from "class-validator";

/**
 * @swagger
 * definitions:
 *   Rank:
 *     type: object
 *     properties:
 *       id:
 *         type: integer
 *       name:
 *         type: string
 */

@Entity()
export class Rank {

    @PrimaryGeneratedColumn()
    id!: number;

    @IsAlpha()
    @Column({length: 100})
    name!: string;

    @OneToMany(type => User, user => user.rank)
    user!: User[]
}
