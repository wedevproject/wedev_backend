import swaggerUi from "swagger-ui-express";
import swaggerJSDoc from "swagger-jsdoc";
import options from "../config/swaggerDefinition.json";
import Synox from "../Synox";

const specs = swaggerJSDoc(options);

export const handleAPIDocs = () => {
    Synox.getExpress().use("/docs", swaggerUi.serve, swaggerUi.setup(specs, {explorer: true}));
};
