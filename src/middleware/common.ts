import cors from "cors";
import parser from "body-parser";
import compression from "compression";
import Synox from "../Synox";

export const handleCors = () =>
    Synox.getExpress().use(cors({ credentials: true, origin: true }));

export const handleBodyRequestParsing = () => {
    Synox.getExpress().use(parser.urlencoded({ extended: true }));
    Synox.getExpress().use(parser.json());
};

export const handleCompression = () => {
    Synox.getExpress().use(compression());
};
