import { Request, Response, NextFunction } from "express";
import { HTTP400Error } from "../utils/httpErrors";

export const checkSearchParams = (req: Request, res: Response, next: NextFunction) => {
    if (!req.query.q) {
        throw new HTTP400Error("Missing q parameter");
    } else {
        next();
    }
};

export const checkParams = (requiredField: string[], req: Request, res: Response, next: NextFunction) => {
    // console.log(req);
    requiredField.map(value => {
        if (!req.body[value]) throw new HTTP400Error(`Missing ${value} parameter`);
    });
    next();
};
