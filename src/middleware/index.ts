import {handleCors, handleBodyRequestParsing, handleCompression} from "./common";
import {handleLog} from "./winston"
import { handleAPIDocs } from "./apiDocs";

export default [
    handleLog,
    handleCors,
    handleBodyRequestParsing,
    handleCompression,
    handleAPIDocs,
];
