import Synox from "../Synox";
import url, {UrlWithStringQuery} from "url";
import { Request, Response, NextFunction } from "express";

export const handleLog = () => {
    Synox.getExpress().use((req: Request, res: Response, next: NextFunction) => {
        const requestedUrl: UrlWithStringQuery = url.parse(req.originalUrl),
            requestEnd = res.end,
            startTime: Date = new Date();

        res.end = ((data: any, encoding: string) => {
            const token: string[] = [
                startTime.toLocaleTimeString().gray,
                colorStatus(res.statusCode),
                methodColor(req.method),
                (new Date().getTime() - startTime.getTime()) + "ms",
                (requestedUrl.path || '').toString(),
                (req.headers['x-forwarded-for'] || req.ip || req.connection.remoteAddress || '').toString()
            ];

            res.end = requestEnd;
            res.end(data, encoding);

            Synox.getLogger().info(token.join(' '));
        }) as any;
        next();
    });
};


function colorStatus(status: number){
    if (status < 400)
        return (status).toString().green;

    if (status < 500)
        return (status).toString().yellow;

    return (status).toString().red;
}


function methodColor(method: string){
    switch (method) {
        case 'GET':
            return method.green;
        case 'POST':
            return method.yellow;
        case 'PUT':
            return method.blue;
        case 'PATCH':
            return method.grey;
        case 'DELETE':
            return method.red;
        default:
            return method.black;
    }
}
