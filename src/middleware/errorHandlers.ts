import { Request, Response, NextFunction } from "express";
import * as ErrorHandler from "../utils/ErrorHandler";
import Synox from "../Synox";

const handle404Error = () => {
    Synox.getExpress().use((req: Request, res: Response) => {
        ErrorHandler.notFoundError();
    });
};

const handleClientError = () => {
    Synox.getExpress().use((err: Error, req: Request, res: Response, next: NextFunction) => {
        ErrorHandler.clientError(err, res, next);
    });
};

const handleServerError = () => {
    Synox.getExpress().use((err: Error, req: Request, res: Response, next: NextFunction) => {
        ErrorHandler.serverError(err, res, next);
    });
};

export default [handle404Error, handleClientError, handleServerError];
