import {NextFunction, Request, Response} from "express";
import {HTTP400Error} from "../utils/httpErrors";
import fs from "fs";
import path from "path";
import jwt from "jsonwebtoken";

export const privateKey = fs.readFileSync(path.join(process.env.PRIVATE_KEY_PATH || '')).toString('utf-8');

export const validateJWT = (req: Request, res: Response, next: NextFunction) => {
    const token:string | undefined = req.header("jwt");
    if (!token) throw new HTTP400Error('Missing JWT');
    jwt.verify(token, privateKey, { algorithms: ['RS256'] }, function (err, payload) {
        if (err) throw new HTTP400Error(err.message);
        next();
    });
};
