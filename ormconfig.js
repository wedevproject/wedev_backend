const host = process.env.HOST || 'localhost';
const password = process.env.PASSWORD || '';

module.exports = {
   "type": "mysql",
   "host": host,
   "port": 3306,
   "username": "root",
   "password": password,
   "database": "synox",
   "synchronize": true,
   "logging": true,
   "entities": [
      __dirname + "/dist/entity/*.js"
   ],
   "migrations": [
      __dirname + "/dist/migration/*.js"
   ],
   "subscribers": [
      __dirname + "/dist/subscriber/*.js"
   ],
   "cli": {
      "entitiesDir": "src/entity",
      "migrationsDir": "src/migration",
      "subscribersDir": "src/subscriber"
   }
};
